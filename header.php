<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
		<div class="container">
		  <div class="jumbotron">
		    <h1>Szkolenie - Polcode</h1>      
		    <h2>Gulp + Bower + LESS</h2>      
		    <h3>Konrad Bolek</h3>
		    <hr>
		    <img src="<?php echo get_template_directory_uri() . '/images/logo.jpg' ?>" alt="Dorzuć logo do images" />
		    <hr>
		    <hr>

		    <ol> 	
				<li>Zainstalować gulp'a</li>
				<li>Przeglądnąć taski gulp'a</li>
				<li>Skompilować body.scss z themem underscore (katalog /underscore/sass/)</li>
				<li>Skompilować jquery.js z themem underscore (katalog /underscore/js/)</li>
				<li>Zmniejsz logo polcode (katalog /underscore/images/)</li>
		    </ol>
		    <p>Wszystkie pliki znajdują się w katalogu _data</p>
		  </div>      
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
